package org.example.jitbinding;

import com.google.inject.Inject;

public class TestClass {
    private String name;

    @Inject
    public TestClass(String name) {
        this.name = name;
    }
}
