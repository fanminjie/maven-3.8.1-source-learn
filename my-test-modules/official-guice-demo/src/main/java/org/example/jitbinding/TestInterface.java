package org.example.jitbinding;

import com.google.inject.ImplementedBy;

@ImplementedBy(TestInterfaceImpl.class)
interface TestInterface {
}
