package org.example.jitbinding;

import com.google.inject.*;

import java.util.logging.Logger;

/**
 * https://github.com/google/guice/wiki/JustInTimeBindings
 */
public class App {



    public static void main(String[] args) {
        AbstractModule module = new AbstractModule() {
            @Override
            public void configure() {
                final Binder binder = binder();

            }
        };
        Injector injector = Guice.createInjector(module);

        // 不显式绑定，在目标类中增加@inject构造器
        TestClass obj = injector.getInstance(TestClass.class);
        System.out.println(obj);

        // 不显式绑定，在目标接口中增加@ImplementBy注解，指定实现类
        TestInterface testInterface = injector.getInstance(TestInterface.class);
        System.out.println(testInterface);

        // ProvidedBy 和 ImplementedBy 注解同理，这里就不演示了

    }
}
