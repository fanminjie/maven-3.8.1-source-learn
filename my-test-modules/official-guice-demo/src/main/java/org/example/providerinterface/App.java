package org.example.providerinterface;

import com.google.inject.*;
import com.google.inject.name.Names;

public class App {
    public static void main(String[] args) {
        AbstractModule module = new AbstractModule() {
            @Override
            public void configure() {
                final Binder binder = binder();
                binder.bind(DatabaseProvider.DataBase.class)
                        .toProvider(DatabaseProvider.class);
            }
        };
        Injector injector = Guice.createInjector(module);


        DatabaseProvider.DataBase obj = injector.getInstance(DatabaseProvider.DataBase.class);
        DatabaseProvider.DataBase obj2 = injector.getInstance(DatabaseProvider.DataBase.class);
        System.out.println(obj);
        System.out.println(obj2);

    }
}
