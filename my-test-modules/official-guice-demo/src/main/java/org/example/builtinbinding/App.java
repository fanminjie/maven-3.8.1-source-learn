package org.example.builtinbinding;

import com.google.inject.*;

import java.util.logging.Logger;

/**
 * https://github.com/google/guice/wiki/BuiltInBindings
 */
public class App {
    private final Logger logger;

    private Injector injector;

    @Inject
    public App(Logger logger, Injector injector) {
        this.logger = logger;
        this.injector = injector;
    }


    public static void main(String[] args) {
        AbstractModule module = new AbstractModule() {
            @Override
            public void configure() {
                final Binder binder = binder();
                binder.bind(App.class);
            }
        };
        Injector injector = Guice.createInjector(module);


        App obj = injector.getInstance(App.class);
        System.out.println(obj.logger);
        obj.logger.info("xxxx");

        System.out.println(obj.injector == injector);
    }
}
