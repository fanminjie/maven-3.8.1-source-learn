package org.example.impl;

import org.example.HelloWorldInterface;

public class HelloWorldInterfaceImpl implements HelloWorldInterface {
    @Override
    public void hello() {
        System.out.println("hello world");
    }
}
